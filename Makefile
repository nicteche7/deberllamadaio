bin/psycopy: obj/main.o
	mkdir -p bin
	gcc -Wall obj/main.o -o bin/psycopy -fsanitize=address,undefined

obj/main.o: src/main.c include/llamadaIO.h
	mkdir -p obj
	gcc -Wall -c src/main.c -o obj/main.o

.PHONY: clean

clean:
	rm bin/* obj/*