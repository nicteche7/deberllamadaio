#include "../include/llamadaIO.h"

int main(int argc, char **argv){

    char *fileReaded = NULL;
    char *fileWrited = NULL;

    if(argc != 3){
        perror("Cantidad de argumentos incorrecto, este programa solo acepta 2 argumentos adicionales, la ruta del archivo origen y destino\n");
        return -1;
    }

    fileReaded = argv[1];
    fileWrited = argv[2];

    char buffer[SIZE] = {0};

    int fd1,fd2 = 0;

    size_t readed, writed, tam = 0;

    fd1 = open(fileReaded, O_RDONLY);

    if(fd1 < 0){
        perror("Error al abrir archivo origen\n");
        return -1;
    }

    umask(0);

    fd2 = open(fileWrited, O_WRONLY | O_CREAT | O_TRUNC, 0666);


	if (fd2 < 0)
	{
		perror("Error al abrir archivo destino\n");
		return -1;
	}

    while((readed = read(fd1,buffer,SIZE)) > 0){
        
        if((writed = write(fd2,buffer,readed)) < 0){
            perror("Error al escribir en archivo destino");
            return -1;
        }
        
        if(lseek(fd2,0,SEEK_END) < 0){
            perror("Error al mover puntero interno del archivo destino");
            return -1;
        }
        

        tam += readed;

        memset(buffer,0,SIZE);
    }

    printf("Se han copiado %ld bytes\n", tam);

    close(fd1);
    close(fd2);

    return 0;
}